$(document).ready(function () {
  $(".orderable").each(function () {
    $(this).append('<span class="material-icons" style="margin-left:5px">unfold_more</span>');
  });


  $(".orderable").children(".material-icons").click(function () {
    if ($(this).text() == "unfold_more") {
      $(this).text("arrow_drop_up"); // Ordena A-Z
    } else if ($(this).text() == "arrow_drop_up") {
      $(this).text("arrow_drop_down");// Ordena Z-A
    } else if ($(this).text() == "arrow_drop_down") {
      $(this).text("unfold_more"); //no ordenado

    }
  });

  $(".ceibo-table-footer .step").click(function(){
    $(".ceibo-table-footer .active-step").removeClass("active-step");
    $(this).addClass("active-step");
  });

  $(".collapse-action").click(function(){
    if($(this).text() == "expand_more")
      $(this).text("expand_less");
    else{
      $(this).text("expand_more");
    }
  });

});
